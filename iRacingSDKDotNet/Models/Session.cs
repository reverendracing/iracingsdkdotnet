﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Session
    {
        public int SessionNum { get; private set; }

        public string SessionLaps { get; private set; }

        public string SessionTime { get; private set; }

        public int SessionNumLapsToAvg { get; private set; }

        public string SessionType { get; private set; }

        public string SessionTrackRubberState { get; private set; }

        public string SessionName { get; private set; }

        public string SessionSubType { get; private set; }

        public int SessionSkipped { get; private set; }

        public int SessionRunGroupsUsed { get; private set; }

        public object[] ResultsPositions { get; private set; }

        public FastLap[] ResultsFastestLap { get; private set; }

        public float ResultsAverageLapTime { get; private set; }

        public int ResultsNumCautionFlags { get; private set; }

        public int ResultsNumCautionLaps { get; private set; }

        public int ResultsNumLeadChanges { get; private set; }

        public int ResultsLapsComplete { get; private set; }

        public int ResultsOfficial { get; private set; }
    }
}