﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Camera
    {
        public int CameraNum { get; private set; }
        public string CameraName { get; private set; }
    }
}