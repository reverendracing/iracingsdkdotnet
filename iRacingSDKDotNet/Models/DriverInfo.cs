﻿using YamlDotNet.Serialization;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class DriverInfo
    {
        public int DriverCarIdx { get; private set; }
        public int PaceCarIdx { get; private set; }
        public float DriverHeadPosX { get; private set; }
        public float DriverHeadPosY { get; private set; }
        public float DriverHeadPosZ { get; private set; }
        public float DriverCarRedLine { get; private set; }
        public int DriverCarEngCylinderCount { get; private set; }
        public float DriverCarFuelKgPerLtr { get; private set; }
        public float DriverCarFuelMaxLtr { get; private set; }
        public float DriverCarMaxFuelPct { get; private set; }
        public float DriverPitTrkPct { get; private set; }
        public float DriverCarEstLapTime { get; private set; }
        public string DriverSetupName { get; private set; }
        public int DriverSetupIsModified { get; private set; }
        public string DriverSetupLoadTypeName { get; private set; }
        public int DriverSetupPassedTech { get; private set; }
        public int DriverIncidentCount { get; private set; }

        public Driver[] Drivers { get; private set; }

        [YamlMember(Alias = "DriverCarSLFirstRPM")]
        public float DriverCarSlFirstRpm { get; private set; }

        [YamlMember(Alias = "DriverCarSLShiftRPM")]
        public float DriverCarSlShiftRpm { get; private set; }

        [YamlMember(Alias = "DriverCarSLLastRPM")]
        public float DriverCarSlLastRpm { get; private set; }

        [YamlMember(Alias = "DriverCarSLBlinkRPM")]
        public float DriverCarSlBlinkRpm { get; private set; }

        [YamlMember(Alias = "DriverCarIdleRPM")]
        public float DriverCarIdleRpm { get; private set; }

        [YamlMember(Alias = "DriverUserID")] public int DriverUserId { get; private set; }
    }
}