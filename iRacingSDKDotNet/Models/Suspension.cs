﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Suspension
    {
        public FrontRearSuspension Front { get; private set; }
        public FrontRearSuspension Rear { get; private set; }

        public WheelSuspension LeftFront { get; private set; }
        public WheelSuspension LeftRear { get; private set; }
        public WheelSuspension RightFront { get; private set; }
        public WheelSuspension RightRear { get; private set; }
    }
}