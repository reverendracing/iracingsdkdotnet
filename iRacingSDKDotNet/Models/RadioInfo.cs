﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class RadioInfo
    {
        public int SelectedRadioNum { get; private set; }
        public Radio[] Radios { get; private set; }
    }
}