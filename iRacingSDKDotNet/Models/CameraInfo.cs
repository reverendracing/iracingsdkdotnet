﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class CameraInfo
    {
        public CameraGroup[] Groups { get; private set; }
    }
}