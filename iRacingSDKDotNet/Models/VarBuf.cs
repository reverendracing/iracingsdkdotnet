﻿using System.IO;

namespace iRacingSDKDotNet.Models
{
    public class VarBuf
    {
        internal VarBuf(BinaryReader reader)
        {
            TickCount = reader.ReadInt32();
            BufferOffset = reader.ReadInt32();

            var _ = reader.ReadBytes(8);
        }

        public int BufferOffset { get; }
        public int TickCount { get; }
    }
}