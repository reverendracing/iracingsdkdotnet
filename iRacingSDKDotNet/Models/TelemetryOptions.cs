﻿namespace iRacingSDKDotNet.Models
{
    public class TelemetryOptions
    {
        public string TelemetryDiskFile { get; private set; }
    }
}