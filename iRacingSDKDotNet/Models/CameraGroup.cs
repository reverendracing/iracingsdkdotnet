﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class CameraGroup
    {
        public int GroupNum { get; private set; }
        public string GroupName { get; private set; }
        public Camera[] Cameras { get; private set; }
    }
}