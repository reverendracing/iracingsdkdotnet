﻿namespace iRacingSDKDotNet.Models
{
    public enum VarType
    {
        Char = 0,
        Bool,
        Int,
        BitField,
        Float,
        Double,
        Count
    }
}