﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Radio
    {
        public int RadioNum { get; private set; }
        public int HopCount { get; private set; }
        public int NumFrequencies { get; private set; }
        public int TunedToFrequencyNum { get; private set; }
        public int ScanningIsOn { get; private set; }

        public Frequency[] Frequencies { get; private set; }
    }
}