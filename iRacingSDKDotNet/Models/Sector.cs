﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Sector
    {
        public int SectorNum { get; private set; }
        public float SectorStartPct { get; private set; }
    }
}