﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class FrontRearSuspension
    {
        public string ToeIn { get; private set; }
        public string CrossWeight { get; private set; }
        public string AntiRollBar { get; private set; }
    }
}