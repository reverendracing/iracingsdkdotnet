﻿namespace iRacingSDKDotNet.Models
{
    public class DataFrame
    {
        public DataFrame(SessionMetaInfo sessionInfo, ITelemetry telemetry)
        {
            SessionInfo = sessionInfo;
            Telemetry = telemetry;
        }

        public SessionMetaInfo SessionInfo { get; }
        public ITelemetry Telemetry { get; }
    }
}