﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class SessionMetaInfo
    {
        public WeekendInfo WeekendInfo { get; private set; }
        public SessionInfo SessionInfo { get; private set; }
        public CameraInfo CameraInfo { get; private set; }
        public RadioInfo RadioInfo { get; private set; }
        public DriverInfo DriverInfo { get; private set; }
        public SplitTimeInfo SplitTimeInfo { get; private set; }
        public CarSetup CarSetup { get; private set; }
        public int SessionUpdateNumber { get; internal set; }
    }
}