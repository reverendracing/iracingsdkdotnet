﻿using YamlDotNet.Serialization;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class WheelSuspension
    {
        public string ColdPressure { get; private set; }
        public string LastHotPressure { get; private set; }
        public string TreadRemaining { get; private set; }
        public string CornerWeight { get; private set; }
        public string RideHeight { get; private set; }
        public string SpringPerchOffset { get; private set; }
        public string BumpStiffness { get; private set; }
        public string ReboundStiffness { get; private set; }
        public string Camber { get; private set; }

        [YamlMember(Alias = "LastTempsOMI")] public string LastTempsOmi { get; private set; }
    }
}