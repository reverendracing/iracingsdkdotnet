﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class SplitTimeInfo
    {
        public Sector[] Sectors { get; private set; }
    }
}