﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class WeekendOptions
    {
        public int NumStarters { get; private set; }

        public string StartingGrid { get; private set; }

        public string QualifyScoring { get; private set; }

        public string CourseCautions { get; private set; }

        public int StandingStart { get; private set; }

        public string Restarts { get; private set; }

        public string WeatherType { get; private set; }

        public string Skies { get; private set; }

        public string WindDirection { get; private set; }

        public string WindSpeed { get; private set; }

        public string WeatherTemp { get; private set; }

        public string RelativeHumidity { get; private set; }

        public string FogLevel { get; private set; }

        public string TimeOfDay { get; private set; }

        public string Date { get; private set; }

        public int Unofficial { get; private set; }

        public string CommercialMode { get; private set; }

        public string NightMode { get; private set; }

        public int IsFixedSetup { get; private set; }

        public string StrictLapsChecking { get; private set; }

        public int HasOpenRegistration { get; private set; }

        public int HardcoreLevel { get; private set; }

        public int NumJokerLaps { get; private set; }

        public string IncidentLimit { get; private set; }
    }
}