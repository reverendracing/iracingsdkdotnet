﻿using YamlDotNet.Serialization;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class Frequency
    {
        public int FrequencyNum { get; private set; }
        public string FrequencyName { get; private set; }
        public int Priority { get; private set; }
        public int CarIdx { get; private set; }
        public int EntryIdx { get; private set; }
        public int CanScan { get; private set; }
        public int CanSquawk { get; private set; }
        public int Muted { get; private set; }
        public int IsMutable { get; private set; }
        public int IsDeletable { get; private set; }

        [YamlMember(Alias = "ClubID")] public int ClubId { get; private set; }
    }
}