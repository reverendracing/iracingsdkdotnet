﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

using YamlDotNet.Serialization;

namespace iRacingSDKDotNet.Models
{
    public class Driver
    {
        public int DriverIdx { get; private set; }
        public string UserName { get; private set; }
        public string AbbrevName { get; private set; }
        public string Initials { get; private set; }
        public string TeamName { get; private set; }
        public string CarNumber { get; private set; }
        public int CarNumberRaw { get; private set; }
        public string CarPath { get; private set; }
        public int CarIsPaceCar { get; private set; }
        public string CarScreenName { get; private set; }
        public string CarScreenNameShort { get; private set; }
        public string CarClassShortName { get; private set; }
        public int CarClassRelSpeed { get; private set; }
        public int CarClassLicenseLevel { get; private set; }
        public string CarClassMaxFuelPct { get; private set; }
        public string CarClassWeightPenalty { get; private set; }
        public string CarClassColor { get; private set; }
        public int LicLevel { get; private set; }
        public int LicSubLevel { get; private set; }
        public string LicString { get; private set; }
        public string LicColor { get; private set; }
        public int IsSpectator { get; private set; }
        public string CarDesignStr { get; private set; }
        public string HelmetDesignStr { get; private set; }
        public string SuitDesignStr { get; private set; }
        public string CarNumberDesignStr { get; private set; }
        public int CurDriverIncidentCount { get; private set; }
        public int TeamIncidentCount { get; private set; }

        // ReSharper disable once InconsistentNaming
        public int IRating { get; private set; }

        [YamlMember(Alias = "UserID")] public int UserId { get; private set; }

        [YamlMember(Alias = "TeamID")] public int TeamId { get; private set; }

        [YamlMember(Alias = "CarClassID")] public int CarClassId { get; private set; }

        [YamlMember(Alias = "CarID")] public int CarId { get; private set; }

        [YamlMember(Alias = "CarIsAI")] public int CarIsAi { get; private set; }

        [YamlMember(Alias = "CarSponsor_1")] public int CarSponsor1 { get; private set; }

        [YamlMember(Alias = "CarSponsor_2")] public int CarSponsor2 { get; private set; }
    }
}