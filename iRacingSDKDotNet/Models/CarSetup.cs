﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class CarSetup
    {
        public int UpdateCount { get; private set; }

        public Suspension Suspension { get; private set; }
    }
}