﻿using System.IO;
using System.Linq;

namespace iRacingSDKDotNet.Models
{
    public class SdkHeader
    {
        private const int MaxBufs = 4;

        public SdkHeader(BinaryReader reader)
        {
            Version = reader.ReadInt32();
            Status = reader.ReadInt32();
            TickRate = reader.ReadInt32();

            SessionInfoUpdate = reader.ReadInt32();
            SessionInfoLen = reader.ReadInt32();
            SessionInfoOffset = reader.ReadInt32();

            NumVars = reader.ReadInt32();
            VarHeaderOffset = reader.ReadInt32();

            NumBuf = reader.ReadInt32();
            BufLen = reader.ReadInt32();
            var _ = reader.ReadBytes(8);

            VarBufs = new VarBuf[MaxBufs];
            for (var i = 0; i < VarBufs.Length; i++)
            {
                VarBufs[i] = new VarBuf(reader);
            }
        }

        public VarBuf[] VarBufs { get; }

        public int BufLen { get; }

        public int NumBuf { get; }

        public int SessionInfoLen { get; }

        public int SessionInfoOffset { get; }

        public int SessionInfoUpdate { get; }

        public int Status { get; }

        public int Version { get; }

        public int TickRate { get; }

        public int VarHeaderOffset { get; }

        public int NumVars { get; }

        public VarBuf FindLastBuf()
        {
            return VarBufs.OrderBy(buf => buf.TickCount).Reverse().FirstOrDefault();
        }
    }
}