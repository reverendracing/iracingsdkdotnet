﻿using System.IO;

namespace iRacingSDKDotNet.Models
{
    public class VarHeader
    {
        private const int MaxString = 32;
        private const int MaxDesc = 64;

        internal VarHeader(BinaryReader reader)
        {
            Type = (VarType) reader.ReadInt32();
            Offset = reader.ReadInt32();
            Count = reader.ReadInt32();
            CountAsTime = reader.ReadBoolean();

            var _ = reader.ReadBytes(3);

            Name = new string(reader.ReadChars(MaxString)).Trim('\0');
            Description = new string(reader.ReadChars(MaxDesc)).Trim('\0');
            Unit = new string(reader.ReadChars(MaxString)).Trim('\0');
        }

        public int Count { get; }

        public bool CountAsTime { get; }

        public string Description { get; }

        public string Name { get; }

        public int Offset { get; }

        public VarType Type { get; }

        public string Unit { get; }
    }
}