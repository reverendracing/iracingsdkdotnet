﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class FastLap
    {
        public int CarIdx { get; private set; }
        public int FastestLap { get; private set; }
        public float FastestTime { get; private set; }
    }
}