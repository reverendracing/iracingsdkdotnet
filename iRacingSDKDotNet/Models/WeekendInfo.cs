﻿using YamlDotNet.Serialization;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace iRacingSDKDotNet.Models
{
    public class WeekendInfo
    {
        [YamlMember(Alias = "SeriesID")] public int SeriesId { get; private set; }

        [YamlMember(Alias = "SeasonID")] public int SeasonId { get; private set; }

        [YamlMember(Alias = "SessionID")] public int SessionId { get; private set; }

        [YamlMember(Alias = "SubsessionID")] public int SubsessionId { get; private set; }

        [YamlMember(Alias = "LeagueID")] public int LeagueId { get; private set; }

        public int Official { get; private set; }

        public int RaceWeek { get; private set; }

        public string EventType { get; private set; }

        public string Category { get; private set; }

        public string SimMode { get; private set; }

        public int TeamRacing { get; private set; }

        public int MinDrivers { get; private set; }

        public int MaxDrivers { get; private set; }

        [YamlMember(Alias = "DCRuleSet")] public string DcRuleSet { get; private set; }

        public int QualifierMustStart { get; private set; }

        public int NumCarClasses { get; private set; }

        public int NumCarTypes { get; private set; }

        public int HeatRacing { get; private set; }

        public WeekendOptions WeekendOptions { get; private set; }

        public TelemetryOptions TelemetryOptions { get; private set; }

        #region Track

        public string TrackName { get; private set; }

        [YamlMember(Alias = "TrackID")] public int TrackId { get; private set; }

        public string TrackLength { get; private set; }

        public string TrackDisplayName { get; private set; }

        public string TrackDisplayShortName { get; private set; }

        public string TrackConfigName { get; private set; }

        public string TrackCity { get; private set; }

        public string TrackCountry { get; private set; }

        public string TrackAltitude { get; private set; }

        public string TrackLatitude { get; private set; }

        public string TrackLongitude { get; private set; }

        public string TrackNorthOffset { get; private set; }

        public int TrackNumTurns { get; private set; }

        public string TrackPitSpeedLimit { get; private set; }

        public string TrackType { get; private set; }

        public string TrackDirection { get; private set; }

        public string TrackWeatherType { get; private set; }

        public string TrackSkies { get; private set; }

        public string TrackSurfaceTemp { get; private set; }

        public string TrackAirTemp { get; private set; }

        public string TrackAirPressure { get; private set; }

        public string TrackWindVel { get; private set; }

        public string TrackWindDir { get; private set; }

        public string TrackRelativeHumidity { get; private set; }

        public string TrackFogLevel { get; private set; }

        public int TrackCleanup { get; private set; }

        public int TrackDynamicTrack { get; private set; }

        #endregion
    }
}