﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using Castle.Components.DictionaryAdapter;
using iRacingSDKDotNet.Models;
using YamlDotNet.Serialization;

namespace iRacingSDKDotNet
{
    public class IRacingConnection : IDisposable
    {
        // Cache YamlDotNet deserializer
        private static readonly IDeserializer Deserializer;

        // Cache Castle dictionary adapter factory
        private static readonly DictionaryAdapterFactory DictionaryAdapterFactory;

        private readonly IMemoryAccessor _memoryAccessor;

        // Cache SessionMetaInfo which only updates periodically
        private SessionMetaInfo _lastSessionInfo;

        // Last session info update number. Changes each time the session info changes.
        private int _sessionLastInfoUpdate;

        // Generates a DataFrame as soon as they are available
        public IObservable<DataFrame> DataFrames;

        // Generates a SessionMetaInfo whenever new data is available
        public IObservable<SessionMetaInfo> SessionMetaInfo;

        static IRacingConnection()
        {
            // Make sure to ignore unmatched properties in the session info, to not fail on new builds
            Deserializer = new DeserializerBuilder().IgnoreUnmatchedProperties().Build();

            // Initialize a one-time dictionary adapter factory
            DictionaryAdapterFactory = new DictionaryAdapterFactory();
        }

        public IRacingConnection(IMemoryAccessor memoryAccessor)
        {
            _memoryAccessor = memoryAccessor;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            // Dispose of the MMF resource
            _memoryAccessor.Dispose();
        }

        /// <summary>
        ///     Connect to the iRacing process and initialize DataFrames
        /// </summary>
        /// <returns>True if successfully connected, false otherwise</returns>
        public bool Connect()
        {
            if (!_memoryAccessor.IsConnected)
            {
                return false;
            }

            DataFrames = _memoryAccessor.Memory.SubscribeOn(ThreadPoolScheduler.Instance).Select(ParseMemoryMappedFile);
            SessionMetaInfo = DataFrames.Select(d => d.SessionInfo).OnDistinct(s => s.SessionUpdateNumber);

            return true;
        }

        /// <summary>
        ///     Parse the memory mapped file into a DataFrame
        /// </summary>
        /// <returns>The current data frame</returns>
        private DataFrame ParseMemoryMappedFile(byte[] input)
        {
            // Create a memory stream from the MMF
            using (var byteStream = new MemoryStream(input))
            {
                // Create a binary reader from the MMF stream
                using (var reader = new BinaryReader(byteStream))
                {
                    // Use the reader to initialize the SDK Header
                    var header = new SdkHeader(reader);

                    // Initialize the array of variable headers
                    var varHeaders = new VarHeader[header.NumVars];

                    // Seek to var headers in stream
                    reader.BaseStream.Seek(header.VarHeaderOffset, SeekOrigin.Begin);

                    // Parse all variable headers
                    for (var i = 0; i < varHeaders.Length; i++)
                    {
                        // The variable headers are sequential in memory
                        // Use the binary reader directly to initialize
                        varHeaders[i] = new VarHeader(reader);
                    }

                    // Seek to beginning of session info
                    reader.BaseStream.Seek(header.SessionInfoOffset, SeekOrigin.Begin);

                    // Parse session info from the memory stream
                    var sessionInfo = ParseSessionInfo(header, reader);

                    // Parse telemetry data from the binary reader
                    var telemetry = ParseTelemetry(header, varHeaders, reader);

                    return new DataFrame(sessionInfo, telemetry);
                }
            }
        }

        /// <summary>
        ///     Reads SessionInfo from YAML formatted data
        /// </summary>
        /// <param name="header">Header for the current data frame</param>
        /// <param name="reader">The reader for the current data frame</param>
        /// <returns>All session meta info from the YAML data</returns>
        private SessionMetaInfo ParseSessionInfo(SdkHeader header, BinaryReader reader)
        {
            // If there has been no update to the session info just return the last parsed version
            if (header.SessionInfoUpdate == _sessionLastInfoUpdate)
            {
                return _lastSessionInfo;
            }

            // Update the last session info update number
            _sessionLastInfoUpdate = header.SessionInfoUpdate;

            // Read all bytes of the session info
            var sessionInfoData = reader.ReadBytes(header.SessionInfoLen);

            // Parse bytes of session info into a string
            var sessionInfoString = Encoding.Default.GetString(sessionInfoData);

            // Session info string is null terminated. Find the first index of null character
            var nullIndex = sessionInfoString.IndexOf('\0');

            // If there is no null character return null. Data is not valid
            if (nullIndex == -1)
            {
                _lastSessionInfo = null;
                _sessionLastInfoUpdate = -1;
                return _lastSessionInfo;
            }

            // Only care about string before first null character
            sessionInfoString = sessionInfoString.Substring(0, nullIndex);

            // Deserialize YAML session info with YamlDotNet
            _lastSessionInfo = Deserializer.Deserialize<SessionMetaInfo>(sessionInfoString);

            // Set the session update number for the SessionInfo
            _lastSessionInfo.SessionUpdateNumber = _sessionLastInfoUpdate;

            return _lastSessionInfo;
        }

        /// <summary>
        ///     Parse telemetry data from a binary reader
        /// </summary>
        /// <param name="header">Header containing data for the current data frame</param>
        /// <param name="varHeaders">Variable headers for the current data frame</param>
        /// <param name="reader">The reader for the current data frame</param>
        /// <returns>Parsed telemetry data</returns>
        private static ITelemetry ParseTelemetry(SdkHeader header,
            IEnumerable<VarHeader> varHeaders,
            BinaryReader reader)
        {
            // Initialize the telemetry dictionary
            var telemetryVarMap = new Dictionary<string, object>();

            // Find the newest variable buffer in the header
            var buf = header.FindLastBuf();
            if (buf == null)
            {
                return null;
            }

            // Read every header from the variable headers array
            foreach (var varHeader in varHeaders)
            {
                // Calculate the offset to read the variable header in the last buffer
                var offset = buf.BufferOffset + varHeader.Offset;

                // Seek in the base stream only if we aren't at the right position
                if (offset != reader.BaseStream.Position)
                {
                    reader.BaseStream.Seek(offset, SeekOrigin.Begin);
                }

                // Add the variable to the telemetry dictionary
                // If there is only a single value, just read the given type from the stream
                // If there are multiple values read all of them into an array
                // TODO: Investigate a better way to do this with less repetition
                telemetryVarMap.Add(varHeader.Name,
                    varHeader.Count != 1
                        ? ReadMultipleValues(reader, varHeader.Count, varHeader.Type)
                        : GetSingleValueReaderFor(varHeader.Type)(reader));
            }

            return DictionaryAdapterFactory.GetAdapter<ITelemetry>(telemetryVarMap);
        }

        /// <summary>
        ///     Generates the reader function for all <see cref="VarType" /> values
        /// </summary>
        /// <param name="type">The type of variable for which a reader function is needed</param>
        /// <returns>The reader function for the given <see cref="VarType" /></returns>
        private static Func<BinaryReader, object> GetSingleValueReaderFor(VarType type)
        {
            switch (type)
            {
                case VarType.Int:
                case VarType.BitField:
                    return reader => reader.ReadInt32();
                case VarType.Double:
                    return reader => reader.ReadDouble();
                case VarType.Bool:
                    return reader => reader.ReadBoolean();
                case VarType.Float:
                    return reader => reader.ReadSingle();
                case VarType.Char:
                    return reader => reader.ReadChar();
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        /// <summary>
        ///     Read a certain number of values of a given type to an array
        /// </summary>
        /// <param name="reader">The reader to be read from</param>
        /// <param name="count">The number of items to be read</param>
        /// <param name="type">The type of the items being read</param>
        /// <returns>An array of read items</returns>
        private static object[] ReadMultipleValues(BinaryReader reader, int count, VarType type)
        {
            var singleReader = GetSingleValueReaderFor(type);

            var data = new object[count];
            for (var i = 0; i < count; i++)
            {
                data[i] = singleReader(reader);
            }

            return data;
        }
    }
}