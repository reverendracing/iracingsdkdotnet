﻿using System;

namespace iRacingSDKDotNet
{
    public interface IMemoryAccessor : IDisposable
    {
        IObservable<byte[]> Memory { get; }
        bool IsConnected { get; }
    }
}