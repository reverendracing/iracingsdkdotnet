﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;

namespace iRacingSDKDotNet
{
    public static class ObservableExtensions
    {
        public static IEnumerable<T> OnDistinct<T>(this IEnumerable<T> self)
        {
            return self.OnDistinct(v => v);
        }

        public static IEnumerable<T> OnDistinct<T, TSelect>(this IEnumerable<T> self, Func<T, TSelect> selector)
        {
            TSelect last = default;
            foreach (var val in self)
            {
                var sel = selector(val);
                if (last == null || last.Equals(sel)) continue;

                last = sel;
                yield return val;
            }
        }

        public static IObservable<T> OnDistinct<T>(this IObservable<T> self)
        {
            return self.Distinct(v => v);
        }

        public static IObservable<T> OnDistinct<T, TSelect>(this IObservable<T> self, Func<T, TSelect> selector)
        {
            return self.Scan((prev: default(TSelect), current: default(T)),
                    (tup, next) => (tup.current == null ? default : selector(tup.current), next))
                .Where(t =>
                {
                    var (prev, current) = t;
                    return current != null && !selector(current).Equals(prev);
                })
                .Select(t => t.current);
        }
    }
}