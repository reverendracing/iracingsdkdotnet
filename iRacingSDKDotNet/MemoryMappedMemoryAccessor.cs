﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Reactive.Linq;
using System.Threading;

namespace iRacingSDKDotNet
{
    public class MemoryMappedMemoryAccessor : IMemoryAccessor
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private readonly MemoryMappedFile _irMemory;

        public MemoryMappedMemoryAccessor()
        {
            // Open the iRacing EventHandle
            var eventExists = EventWaitHandle.TryOpenExisting(@"Local\IRSDKDataValidEvent", out var dataValidEvent);

            // If the EventHandle doesn't exist it is not possible to start reading
            // Fail the connection attempt
            if (!eventExists)
            {
                IsConnected = false;
                return;
            }

            try
            {
                // Open the MMF
                _irMemory = MemoryMappedFile.OpenExisting(@"Local\IRSDKMemMapFileName");
            }
            catch (Exception)
            {
                // If MMF can't be opened reading it is not possible
                IsConnected = false;
                return;
            }

            Memory = Observable.Generate(dataValidEvent, waitHandle => waitHandle.WaitOne(), waitHandle => waitHandle,
                _ => GetMemoryBytes());
            IsConnected = true;
        }

        private byte[] GetMemoryBytes()
        {
            using (var irMemoryStream = _irMemory.CreateViewStream())
            {
                using (var byteMemoryStream = new MemoryStream((int) irMemoryStream.Length))
                {
                    irMemoryStream.CopyTo(byteMemoryStream);
                    return byteMemoryStream.ToArray();
                }
            }
        }

        public IObservable<byte[]> Memory { get; }
        public bool IsConnected { get; }
    }
}