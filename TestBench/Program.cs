﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using iRacingSDKDotNet;
using iRacingSDKDotNet.Models;

namespace TestBench
{
    internal static class Program
    {
        private static void Main()
        {
            var connection = new IRacingConnection(new MemoryMappedMemoryAccessor());
            if (connection.Connect())
            {
                connection.DataFrames
                    .OnDistinct(d => d.Telemetry.Lap)
                    .Subscribe(d => { Console.WriteLine($"Telemetry: {d.Telemetry.Speed * 3.6}"); });

                connection.DataFrames
                    .Select(d => d.SessionInfo)
                    .OnDistinct(s => s.SessionUpdateNumber)
                    .Subscribe(s => { Console.WriteLine($"SessionInfo: {s.SessionUpdateNumber}"); });
            }
            else
            {
                Console.WriteLine("iRacing not running. Could not connect!");
            }

            Console.ReadLine();
        }
    }
}