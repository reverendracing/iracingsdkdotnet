using System.Collections.Generic;
using System.Reactive.Linq;
using Xunit;

namespace iRacingSDKDotNet.Tests
{
    public class OnDistinct
    {
        [Fact]
        public void PassesThroughDistinctValues()
        {
            var actual = Observable.Range(1, 10).OnDistinct().ToEnumerable();
            var expected = new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FiltersOutEqualValues()
        {
            var actual = Observable.Range(1, 10)
                .Select(i => i % 2 == 0 ? i + 1 : i)
                .OnDistinct()
                .ToEnumerable();
            var expected = new List<int> {1, 3, 5, 7, 9, 11};

            Assert.Equal(expected, actual);
        }
    }
}