# iRacingSDKDotNet

A reactive SDK for iRacing

This is a fork of [iRacingSDK.NET by Dean Netherton](https://github.com/vipoo/iRacingSDK.Net)

# Usage

To establish the most basic connection to iRacing two components are required. An `IRacingConnection` instance and an `IObserver` implementation for the `DataFrame` class.

Through the `IRacingConnection` class an observable stream of `DataFrame` objects are available, and can be subscribed on. This is shown in the below example.

```csharp
using iRacingSDKDotNet;

class Program
{
    private static void Main()
    {
        var connection = new IRacingConnection();
        if (connection.Connect())
        {
            connection.DataFrames.Subscribe(new Observer());
        }
        else
        {
            Console.WriteLine("iRacing not running. Could not connect!");
        }
    }
}
```

```csharp
using iRacingSDKDotNet.Models;

class Observer : IObserver<DataFrame>
{
    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
    }

    public void OnNext(DataFrame value)
    {
        Console.WriteLine(value.SessionInfo.WeekendInfo.TrackDisplayName);
    }
}
```

# Details

iRacing creates a Windows event which is triggered each time the telemetry info is updated. Using a Observable generator we read the telemetry info each time the event is triggered, and propogate it into the observable collection.
